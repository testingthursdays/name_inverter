Name Inverter
=============

Write a function that receives a name and rewrites that name, last name first separated by a comma.

Definition
==========

- The function must treat invalid input by returning a empty string;
- The function must remove extra spaces from the names;
- Middle names are to be ignored;
- Connectives must be placed as is in the correct order;
- Honorifics must be ignored;
- Postnominals must be appended after the inverted string, in order;
 
Clarifications
---------------
- *Connectives* - Are connectives to the last name: **do** Valle 
- *Honorifics* - Are "Mr.", "Ms.", "Mrs." that comes **before** the name;
- *Postnominals* - "Phd.", "Msc.", "Jr", "Md.", etc that comes **after** the name.
 
Test Suggestions
=============
 
- John Smith -> Smith, John
- Mrs. Jane Foster - > Foster, Jane
- Robert Brown Jr. -> Brown, Robert Jr.
- Mr. Mark Spencer Dustin Jr. Md. -> Dustin, Mark Jr. Md.